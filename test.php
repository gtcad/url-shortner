<?php
try{
    //Очевидно, 1 никогда не будет равняться 2...
    if(1 !== 2){
        //Генерируем исключение.
        throw new Exception('1 не равняется 2!');
    }
}
//Перехватываем (catch) исключение, если что-то идет не так.
catch (Exception $ex) {
    //Выводим сообщение об исключении.
    echo $ex->getMessage() . "\n";
}
echo "after catch \n";



try {
    // init bootstrapping phase

    $config_file_path = "config.php";

    if (!file_exists($config_file_path))
    {
        throw new Exception("");
    }

    // continue execution of the bootstrapping phase
} catch (Exception $e) {
    echo $e->getMessage();
    die();
}

if (!file_exists($config_file_path))
{
    die();
}



