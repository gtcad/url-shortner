<?php

class Setup
{
    public function createTable()
    {
    	global $mysqli;
		$word = 'a';
		for ($i = 0; $i < 26; $i++) {
			$mysqli->query("CREATE TABLE `" . $word . "` (`id` INT(11) NOT NULL AUTO_INCREMENT,`url` VARCHAR(1024) NOT NULL,`data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,PRIMARY KEY (`id`))COLLATE='utf8_general_ci'ENGINE=MyISAM;");
			
			$word = ++$word;
			if ($mysqli->errno != 0) {
				die('sql error: ' . $mysqli->errno);
				return false; 
			}
		}
		echo 'Tables Created' . "\n";
		return true;
    }

    public function createCfg()
    {
    	$settings = [
    	    'tableWord' => 'a',
    	    'apiKey' => $this->randomString(),
    	    'telegrammKey' => '',
    	    'telegrammChat' => 375198929,
    	    'url' => $_SERVER["HTTP_HOST"] . '/',
    	    'errorMode' => false,
    	    'errorLink' => "https://binom.teracock.com/click.php?key=hchvsh3f30psgm1mtt9e&t1=ErrorLink&t2=Shortner"
    	];
    	if (file_put_contents('cfg.json', json_encode($settings))) {
	    	print_r($settings);
	    	echo 'Config Created' . "\n" . "Make this in console: " . "\n\n\n" . "composer require pda/pheanstalk\n" . "sudo aptitude install beanstalkd \n";
			return true;
    	} else {
    		die('Create cfg error!');
    		return false;
    	}
    }
    public function randomString($n = 35) { 
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
	    $randomString = ''; 
	  
	    for ($i = 0; $i < $n; $i++) { 
	        $index = rand(0, strlen($characters) - 1); 
	        $randomString .= $characters[$index]; 
	    } 
	  
	    return $randomString; 
	} 
}

?>