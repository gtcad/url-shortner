<?php

class Create
{

	public function add($json, $word)
	{
		global $cfg;
		//jsonValidate
		if ($json === null) {
			http_response_code(402);
			die("JsonError");
		}
		if (strpos($_SERVER['REQUEST_URI'], "?signature=" . $cfg->apiKey) !== false) {

		} else {
			die("api key is shit");
		}
		//end

		global $mysqli;
		$mysqli->query("LOCK TABLES " . $word . " WRITE;");
		$sql = "INSERT INTO `" . $word . "` (`url`) VALUES";
		foreach ($json->urls as $value) {
			$sql .= "('" . $mysqli->real_escape_string($value) ."'),";
		}
		$mysqli->query(substr($sql, 0, -1));
		$resp = array();
		for ($i = $mysqli->insert_id; $i < $mysqli->insert_id + count($json->urls); $i++) {
			array_push($resp, "https://" . $_SERVER['HTTP_HOST'] . '/' . $word . base_convert($i, 10, 16));
		}
		$mysqli->query("UNLOCK TABLES;");
		$resp = json_encode($resp);
		return $resp;
	}
}

?>