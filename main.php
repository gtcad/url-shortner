<?php
use Pheanstalk\Pheanstalk;


class Redirect
{
	public $keyword;


	private function postback()
	{
		$pheanstalk = Pheanstalk::create('127.0.0.1');
		$pheanstalk
		  ->useTube('pback')
		  ->put($_SERVER['HTTP_HOST'] . '/' .$this->keyword);
	}


	public function main(){
		global $cfg;
		global $mysqli;
		$table = substr($this->keyword, 0, 1);
		$keyword = base_convert(substr($this->keyword, 1), 16, 10);
		echo $keyword;
		$stmt = $mysqli->prepare("SELECT `url` FROM `" . $table . "` WHERE `id` = ?");
      	$stmt->bind_param("s", $keyword);
     	$stmt->execute();
      	$result = $stmt->get_result();
      	if ($result->num_rows != 0) {
      		header("Location: " . $result->fetch_assoc()['url']);
      		$this->postback();
      	} else {
      		header("Location: " . $cfg->errorLink);
      	}
	}

}

?>