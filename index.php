<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$cfg = json_decode(file_get_contents('cfg.json'));
if ($cfg->errorMode != false) {
	header('Location: ' . $cfg->errorLink);
}
$sql = json_decode(file_get_contents('sql.json'));

include $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
include $_SERVER['DOCUMENT_ROOT'] . "/create.php";
include $_SERVER['DOCUMENT_ROOT'] . "/setup.php";
include $_SERVER['DOCUMENT_ROOT'] . "/main.php";

$mysqli = new mysqli($sql->host, $sql->login, $sql->password, $sql->database) or die('sql error');

$data = json_decode(file_get_contents('php://input'));

global $data;
global $mysqli;
global $_GET;
global $cfg;

switch ($_SERVER['REQUEST_URI']) {
	case strpos($_SERVER['REQUEST_URI'], '/create') :
		$main = new Create();
		print_r(
			$main->add($data, $cfg->tableWord)
			);
	break;

	case '/setup':
		if (!$cfg->tableWord) {
			$main = new Setup();
			$main->createTable();
			$main->createCfg();
		} else {
			die("Already installed!");
		}
	break;
	
	default:
		$keyWord = substr($_SERVER['REQUEST_URI'], 1);
		$main = new Redirect();
		$main->keyword = $keyWord;
		$main->main();
	break;
}

?>