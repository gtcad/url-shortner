<?php
include 'vendor/autoload.php';
use Pheanstalk\Pheanstalk;


set_time_limit(0);

ini_set('error_reporting', 1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


$cfg = json_decode(file_get_contents('cfg.json'));
$pheanstalk = Pheanstalk::create('127.0.0.1');
while (true) {
	$job = $pheanstalk
		  ->watch('pback')
		  ->reserve();
	if ($job) {
		echo $job->getData() . "\n";
		$ctx = stream_context_create(array(
			"ssl"=>array(
			    "verify_peer"=>false,
			    "verify_peer_name"=>false,
			   ),
			'http'=> array(
		        'timeout' => 300, 
		      )
		));
		$f = file_get_contents("https://api.teracock.com:7800/u?h=" . $job->getData(), false, $ctx);
		$pheanstalk->delete($job);
	}
}

?>